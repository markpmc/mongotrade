package mongotrade;


/**
 * Created by mark.mcclellan on 4/20/2015.
 */


import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import java.io.IOException;
import java.io.StringReader;
import java.net.URLEncoder;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

public class YDataEODJ {
    private final String USER_AGENT = "Mozilla/5.0";
    static YDataEODJ http = new YDataEODJ();
    static MongoLayerRT ml = new MongoLayerRT();
    static MFConfig cfg = new MFConfig();

    public static void main(String[] args) throws Exception {
        //String symbol = "^gspc";
        //String symbol ="nqu17.cme";
        //String symbol = "CLV15.NYM";
        //String symbol = "EURUSD=X";
        String symbol = cfg.getSymbolString("yahoo");
        String[] sList = symbol.split(",", -1);

       //loop thru the list of sysmbols
        for(String s : sList){
            if(!s.equals("")){
                //export the data as 5 & 30 min bars for GT
                // ml.GTexport(s,20,"M5");
                // ml.GTexport(s, 20, "M30");
               // System.out.println(">>> " + s);
               http.fetchData(s);
            } //end blank symbol
        } //end for symbol


    } //end main

    public void fetchData(String ticker) throws Exception {
        JSONReader jReader = JSONReader.getInstance();

      //  String url1 = "https://query1.finance.yahoo.com/v7/finance/quote?fields=symbol,regularMarketPrice,regularMarketChange,regularMarketChangePercent,regularMarketOpen,regularMarketDayHigh,regularMarketDayLow,regularMarketVolume,regularMarketTime&formatted=false&symbols=";
      //  String url = url1 + URLEncoder.encode(ticker,"UTF-8");

        String url1 = "https://l1-query.finance.yahoo.com/v8/finance/chart/";
        String url2 = "?interval=1d&range=1d&indicators=quote&includeTimestamps=true";
        String url = url1 + URLEncoder.encode(ticker,"UTF-8") + url2;

        System.out.println(url);
        JSONObject jsonData = jReader.getContentFromHttpUri(url);

        http.process_yahoo_json(jsonData);
    } //end fetchData

    public void process_yahoo_json(JSONObject jsonData) throws UnknownHostException, JSONException {

        JSONReader jReader = JSONReader.getInstance();
        boolean b_dirty = false;

        String s_curDay = "";
        //QuoteHeader qheader = new QuoteHeader();
        // BarCache min_quote = new BarCache();
        BarCache day_quote = new BarCache();
        //  BarCache prev_quote = new BarCache();

        QuoteBody qbody = new QuoteBody();
        YMUtils ymutil = new YMUtils();
        List<String> bodyList = new ArrayList<String>(); // or LinkedList<String>();

        //   min_quote.init();
        day_quote.init();
        //   prev_quote.init();

        String day = "";
        String date = "";

        String jPath = "";
        String value = "";
        JSONArray timeArray = new JSONArray();
        JSONArray openArray = new JSONArray();
        JSONArray highArray = new JSONArray();
        JSONArray lowArray = new JSONArray();
        JSONArray closeArray = new JSONArray();
        JSONArray volumeArray = new JSONArray();

        try {
            //get the epoch timestamps
            jPath = "/chart/result[0]/timestamp";
            value = jReader.getValue(jPath, jsonData);
            timeArray = new JSONArray(value);

        } catch (JSONException e) {
            b_dirty = true;
        }

        try {
            //get the close array
            jPath = "/chart/result[0]/indicators/quote[0]/close";
            String cValue = jReader.getValue(jPath, jsonData);
            closeArray = new JSONArray(cValue);

            //get the open array
            jPath = "/chart/result[0]/indicators/quote[0]/open";
            String oValue = jReader.getValue(jPath, jsonData);
            openArray = new JSONArray(oValue);

            //get the high array
            jPath = "/chart/result[0]/indicators/quote[0]/high";
            String hValue = jReader.getValue(jPath, jsonData);
            highArray = new JSONArray(hValue);

            //get the low array
            jPath = "/chart/result[0]/indicators/quote[0]/low";
            String lValue = jReader.getValue(jPath, jsonData);
            lowArray = new JSONArray(lValue);

            //get the volume array
            jPath = "/chart/result[0]/indicators/quote[0]/volume";
            String vValue = jReader.getValue(jPath, jsonData);
            volumeArray = new JSONArray(vValue);

        } catch (JSONException e) {
            System.out.println("something missing");
        }
        //global symbol elements
        //misc
        jPath = "/chart/result[0]/meta/symbol";
        value = jReader.getValue(jPath, jsonData);
        day_quote.setTicker(value);
        day_quote.setSource("Y");
        jPath = "/chart/result[0]/meta/exchangeTimezoneName";
        value = jReader.getValue(jPath, jsonData);
        day_quote.setTz(value);

        // System.out.println(min_quote.getTicker());
        // System.out.println(min_quote.getTz());


        //loop thru array
        for (int i=0; i<timeArray.length(); i++) {
            //System.out.println(i + " time: " + timeArray.getString(i) + " open: " + openArray.getString(i));

            //handle timestamp conversion
            if((!timeArray.isNull(i)) && (!b_dirty)) {
                //day = ymutil.unix2dateUTC(Long.parseLong(timeArray.getString(i)));
                //date = ymutil.unixtodate(Long.parseLong(timeArray.getString(i)));

                day = ymutil.unix2day(Long.parseLong(timeArray.getString(i)));
                day_quote.setDay(ymutil.formatYEODDate(day));

                //System.out.println(i+ " " +day + " " + date);

                try {
                    //get quote elements
                    if (!openArray.isNull(i)) {
                        day_quote.setOpen(Double.parseDouble(openArray.getString(i)));
                    } else {
                        b_dirty = true;
                    }
                } catch (NullPointerException np) {
                    System.out.println("missing something in open");
                    b_dirty = true;
                }

                try {
                    if (!highArray.isNull(i)) {
                        day_quote.setHigh(Double.parseDouble(highArray.getString(i)));
                    } else {
                        b_dirty = true;
                    }
                } catch (NullPointerException np) {
                    System.out.println("missing something in high");
                    b_dirty = true;
                }

                try {
                    if (!lowArray.isNull(i)) {
                        day_quote.setLow(Double.parseDouble(lowArray.getString(i)));
                    } else {
                        b_dirty = true;
                    }
                } catch (NullPointerException np) {
                    System.out.println("missing something in low");
                    b_dirty = true;
                }

                try {
                    if (!closeArray.isNull(i)) {
                        day_quote.setClose(Double.parseDouble(closeArray.getString(i)));
                    } else {
                        b_dirty = true;
                    }
                } catch (NullPointerException np) {
                    System.out.println("missing something in close");
                    b_dirty = true;
                }

                try {
                    if (!volumeArray.isNull(i)) {
                        day_quote.setVolume(Long.parseLong(volumeArray.getString(i)));
                    } else {
                        b_dirty = true;
                    }

                } catch (NumberFormatException np) {
                    System.out.println("missing something in volume");
                    b_dirty = true;
                }

                day_quote.setDay(day);
                day_quote.setType("D");

                //build the _id for the data bar.
                String bar_id = day_quote.getTicker() + ":" + day_quote.getSource() + ":" + date;
                day_quote.setH_id(bar_id);

                //load mongo

                day = day_quote.getDay().substring(0, 8);
                day_quote.setH_id(day_quote.getTicker() + ":" + day_quote.getSource() + ":" + day);

                //Store the daily bar
                if(!b_dirty) {
                    ml.mongo_store_bar(day_quote, false);
                }
                day_quote.init();
                b_dirty = false;
            } //end if timestamp null

        } //end for timeArray


    } //end process y json


    public void process_yahoo_eod(JSONObject jsonData) throws UnknownHostException {

        JSONReader jReader = JSONReader.getInstance();
        boolean b_dirty = false;

        BarCache day_quote = new BarCache();
        YMUtils ymutil = new YMUtils();
        day_quote.init();
        String day = "";
        String jPath = "";
        String value = "";

        //start processing

        day_quote.setType("D");
        day_quote.setSource("Y");
        try {
            //get the epoch ticker
            jPath = "/quoteResponse/result[0]/symbol";
            value = jReader.getValue(jPath, jsonData);
        } catch (JSONException e) {
            b_dirty = true;
        }

        day_quote.setTicker(value);

        try {
            //get the epoch timestamps
            jPath = "/quoteResponse/result[0]/regularMarketTime";
            value = jReader.getValue(jPath, jsonData);
        } catch (JSONException e) {
            b_dirty = true;
        }

        day = ymutil.unix2day(Long.parseLong(value));
        day_quote.setDay(ymutil.formatYEODDate(day));

        try {
            //get the Open
            jPath = "/quoteResponse/result[0]/regularMarketOpen";
            value = jReader.getValue(jPath, jsonData);
        } catch (JSONException e) {
            b_dirty = true;
        }

        day_quote.setOpen(Double.parseDouble(value));

        try {
            //get the Close
            jPath = "/quoteResponse/result[0]/regularMarketPrice";
            value = jReader.getValue(jPath, jsonData);
        } catch (JSONException e) {
            b_dirty = true;
        }
        day_quote.setClose(Double.parseDouble(value));

        try {
            //get the High
            jPath = "/quoteResponse/result[0]/regularMarketDayHigh";
            value = jReader.getValue(jPath, jsonData);
        } catch (JSONException e) {
            b_dirty = true;
        }

        day_quote.setHigh(Double.parseDouble(value));
        try {
            //get the Low
            jPath = "/quoteResponse/result[0]/regularMarketDayLow";
            value = jReader.getValue(jPath, jsonData);
        } catch (JSONException e) {
            b_dirty = true;
        }

        day_quote.setLow(Double.parseDouble(value));
        try {
            //get the volume
            jPath = "/quoteResponse/result[0]/regularMarketVolume";
            value = jReader.getValue(jPath, jsonData);
        } catch (JSONException e) {
            b_dirty = true;
        }

        day_quote.setVolume(Double.parseDouble(value));

        day = day_quote.getDay().substring(0, 8);
        day_quote.setH_id(day_quote.getTicker() + ":" + day_quote.getSource() + ":" + day);

        //Store the daily bar
        ml.mongo_store_bar(day_quote, false);


    } //end process y eod

} // end class
