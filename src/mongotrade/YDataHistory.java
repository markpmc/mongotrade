package mongotrade;


/**
 * Created by mark.mcclellan on 4/20/2015.
 */

//import com.csvreader.CsvReader;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

import java.io.IOException;
import java.io.StringReader;

public class YDataHistory {
    private final String USER_AGENT = "Mozilla/5.0";
    static YDataHistory http = new YDataHistory();
    static MongoLayerRT ml = new MongoLayerRT();
    static MFConfig cfg = new MFConfig();

    public static void main(String[] args) throws Exception {
        String symbol = cfg.getSymbolString("yahoo");
        String[] sList = symbol.split(",", -1);

        //careful. not for 1min bars
    /*    for(String s : sList){
            if(!s.equals("")){
                //export the data as 5 & 30 min bars for GT
                // ml.GTexport(s,20,"M5");
                // ml.GTexport(s, 20, "M30");
                // System.out.println(">>> " + s);
                http.fetchData(s);
            } //end blank symbol
        } //end for symbol
*/
        http.fetchData(symbol);
    } //end main

    public void fetchData(String ticker) throws Exception {
        //http://download.finance.yahoo.com/d/quotes.csv?s=%40%5EDJI,GOOG&f=sd1ohgl1v&e=.csv
        JSONReader jReader = JSONReader.getInstance();

    //    https://l1-query.finance.yahoo.com/v8/finance/chart/%5EGSPC?interval=1d&range=3mo&indicators=quote&includeTimestamps=true
        String url1 = "https://l1-query.finance.yahoo.com/v8/finance/chart/";
    //  String url2 = "?interval=1d&range=6mo&indicators=quote&includeTimestamps=true";
        String url2 = "?interval=1m&range=3d&indicators=quote&includeTimestamps=true";
        String url = url1 + URLEncoder.encode(ticker,"UTF-8") + url2;

        JSONObject jsonData = jReader.getContentFromHttpUri(url);

        //http.process_yahoo_eod(result);
        http.process_yahoo_json(jsonData);

        String[] sList = ticker.split(",", -1);

        for(String s : sList){
            if(!s.equals("")){
                //export the data as 5 & 30 min bars for GT
               // ml.GTexport(s,20,"M5");
               // ml.GTexport(s, 20, "M30");
            } //end blank symbol
        } //end for symbol
    } //end fetchData

    public void process_yahoo_json(JSONObject jsonData) throws UnknownHostException, JSONException {

        JSONReader jReader = JSONReader.getInstance();
        boolean b_dirty = false;

        String s_curDay = "";
        //QuoteHeader qheader = new QuoteHeader();
       // BarCache min_quote = new BarCache();
        BarCache day_quote = new BarCache();
      //  BarCache prev_quote = new BarCache();

        QuoteBody qbody = new QuoteBody();
        YMUtils ymutil = new YMUtils();
        List<String> bodyList = new ArrayList<String>(); // or LinkedList<String>();

     //   min_quote.init();
        day_quote.init();
     //   prev_quote.init();

        String day = "";
        String date = "";

        String jPath = "";
        String value = "";
        JSONArray timeArray = new JSONArray();
        JSONArray openArray = new JSONArray();
        JSONArray highArray = new JSONArray();
        JSONArray lowArray = new JSONArray();
        JSONArray closeArray = new JSONArray();
        JSONArray volumeArray = new JSONArray();

        try {
            //get the epoch timestamps
            jPath = "/chart/result[0]/timestamp";
            value = jReader.getValue(jPath, jsonData);
            timeArray = new JSONArray(value);

        } catch (JSONException e) {
            b_dirty = true;
        }

        try {
            //get the close array
            jPath = "/chart/result[0]/indicators/quote[0]/close";
            String cValue = jReader.getValue(jPath, jsonData);
            closeArray = new JSONArray(cValue);

            //get the open array
            jPath = "/chart/result[0]/indicators/quote[0]/open";
            String oValue = jReader.getValue(jPath, jsonData);
            openArray = new JSONArray(oValue);

            //get the high array
            jPath = "/chart/result[0]/indicators/quote[0]/high";
            String hValue = jReader.getValue(jPath, jsonData);
            highArray = new JSONArray(hValue);

            //get the low array
            jPath = "/chart/result[0]/indicators/quote[0]/low";
            String lValue = jReader.getValue(jPath, jsonData);
            lowArray = new JSONArray(lValue);

            //get the volume array
            jPath = "/chart/result[0]/indicators/quote[0]/volume";
            String vValue = jReader.getValue(jPath, jsonData);
            volumeArray = new JSONArray(vValue);

        } catch (JSONException e) {
            System.out.println("something missing");
        }
        //global symbol elements
        //misc
        jPath = "/chart/result[0]/meta/symbol";
        value = jReader.getValue(jPath, jsonData);
        day_quote.setTicker(value);
        day_quote.setSource("Y");
        jPath = "/chart/result[0]/meta/exchangeTimezoneName";
        value = jReader.getValue(jPath, jsonData);
        day_quote.setTz(value);

        // System.out.println(min_quote.getTicker());
        // System.out.println(min_quote.getTz());


        //loop thru array
        for (int i=0; i<timeArray.length(); i++) {
            //System.out.println(i + " time: " + timeArray.getString(i) + " open: " + openArray.getString(i));

            //handle timestamp conversion
            if((!timeArray.isNull(i)) && (!b_dirty)) {
                //day = ymutil.unix2dateUTC(Long.parseLong(timeArray.getString(i)));
                //date = ymutil.unixtodate(Long.parseLong(timeArray.getString(i)));

                day = ymutil.unix2day(Long.parseLong(timeArray.getString(i)));
                day_quote.setDay(ymutil.formatYEODDate(day));

                //System.out.println(i+ " " +day + " " + date);

                try {
                    //get quote elements
                    if (!openArray.isNull(i)) {
                        day_quote.setOpen(Double.parseDouble(openArray.getString(i)));
                    } else {
                        b_dirty = true;
                    }
                } catch (NullPointerException np) {
                    System.out.println("missing something in open");
                    b_dirty = true;
                }

                try {
                    if (!highArray.isNull(i)) {
                        day_quote.setHigh(Double.parseDouble(highArray.getString(i)));
                    } else {
                        b_dirty = true;
                    }
                } catch (NullPointerException np) {
                    System.out.println("missing something in high");
                    b_dirty = true;
                }

                try {
                    if (!lowArray.isNull(i)) {
                        day_quote.setLow(Double.parseDouble(lowArray.getString(i)));
                    } else {
                        b_dirty = true;
                    }
                } catch (NullPointerException np) {
                    System.out.println("missing something in low");
                    b_dirty = true;
                }

                try {
                    if (!closeArray.isNull(i)) {
                        day_quote.setClose(Double.parseDouble(closeArray.getString(i)));
                    } else {
                        b_dirty = true;
                    }
                } catch (NullPointerException np) {
                    System.out.println("missing something in close");
                    b_dirty = true;
                }

                try {
                    if (!volumeArray.isNull(i)) {
                        day_quote.setVolume(Long.parseLong(volumeArray.getString(i)));
                    } else {
                        b_dirty = true;
                    }

                } catch (NumberFormatException np) {
                    System.out.println("missing something in volume");
                    b_dirty = true;
                }

                day_quote.setDay(day);
                day_quote.setType("D");

                //build the _id for the data bar.
                String bar_id = day_quote.getTicker() + ":" + day_quote.getSource() + ":" + date;
                day_quote.setH_id(bar_id);

                //load mongo

                day = day_quote.getDay().substring(0, 8);
                day_quote.setH_id(day_quote.getTicker() + ":" + day_quote.getSource() + ":" + day);

                //Store the daily bar
                if(!b_dirty) {
                    ml.mongo_store_bar(day_quote, false);
                }
                day_quote.init();
                b_dirty = false;
            } //end if timestamp null

        } //end for timeArray


    } //end process y json

    // HTTP GET request
    private StringBuffer sendGet(String url) throws Exception {

        //String url = "http://chartapi.finance.yahoo.com/instrument/1.0/%5EGSPC/chartdata;type=quote;range=10d/json";

        URL obj = new URL(url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();

        // optional default is GET
        con.setRequestMethod("GET");

        //add request header
        con.setRequestProperty("User-Agent", USER_AGENT);

        int responseCode = con.getResponseCode();
        System.out.println("\nSending 'GET' request to URL : " + url);
        System.out.println("Response Code : " + responseCode);

        BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine + "\n");
            //System.out.println(inputLine);
        }
        in.close();

        return response;

    } //end sendGet

} // end class
