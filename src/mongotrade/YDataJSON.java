package mongotrade;


/**
 * Created by mark.mcclellan on 4/20/2015.
 */

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

public class YDataJSON {
    private final String USER_AGENT = "Mozilla/5.0";
    static YDataJSON http = new YDataJSON();
    static MongoLayerRT ml = new MongoLayerRT();

    public static void main(String[] args) throws Exception {
        String symbol = "^gspc";
        //String symbol ="nqu17.cme";
        //String symbol = "CLV15.NYM";
        //String symbol = "EURUSD=X";
        http.fetchData(symbol);


    } //end main

    public void fetchData(String ticker) throws Exception {
        JSONReader jReader = JSONReader.getInstance();

        String url1 = "https://l1-query.finance.yahoo.com/v8/finance/chart/";
        String url2 = "?interval=1m&range=1d&indicators=quote&includeTimestamps=true";
        String url = url1 + URLEncoder.encode(ticker,"UTF-8") + url2;

        JSONObject jsonData = jReader.getContentFromHttpUri(url);
        //System.out.println("result=" + jsonData);
        System.out.println("url=" + url);


        http.process_yahoo_json(jsonData);



    } //end fetchData



    //receives a String Buffer containing csv data from Yahoo.
    //loops thru processing the header and body.
    //replies upon QuoteBody and QuoteHeader as data structures
    public void process_yahoo_json(JSONObject jsonData) throws UnknownHostException, JSONException {

        JSONReader jReader = JSONReader.getInstance();
        boolean b_dirty = false;

        String s_curDay = "";
        //QuoteHeader qheader = new QuoteHeader();
        BarCache min_quote = new BarCache();
        BarCache day_quote = new BarCache();
        BarCache prev_quote = new BarCache();

        QuoteBody qbody = new QuoteBody();
        YMUtils ymutil = new YMUtils();
        List<String> bodyList = new ArrayList<String>(); // or LinkedList<String>();

        min_quote.init();
        day_quote.init();
        prev_quote.init();

        String day = "";
        String date = "";

        String jPath = "";
        String value = "";
        JSONArray timeArray = new JSONArray();
        JSONArray openArray = new JSONArray();
        JSONArray highArray = new JSONArray();
        JSONArray lowArray = new JSONArray();
        JSONArray closeArray = new JSONArray();
        JSONArray volumeArray = new JSONArray();

        try {
            //get the epoch timestamps
            jPath = "/chart/result[0]/timestamp";
            value = jReader.getValue(jPath, jsonData);
            timeArray = new JSONArray(value);

        } catch (JSONException e) {
            b_dirty = true;
        }

        try {
            //get the close array
            jPath = "/chart/result[0]/indicators/quote[0]/close";
            String cValue = jReader.getValue(jPath, jsonData);
            closeArray = new JSONArray(cValue);

            //get the open array
            jPath = "/chart/result[0]/indicators/quote[0]/open";
            String oValue = jReader.getValue(jPath, jsonData);
            openArray = new JSONArray(oValue);

            //get the high array
            jPath = "/chart/result[0]/indicators/quote[0]/high";
            String hValue = jReader.getValue(jPath, jsonData);
            highArray = new JSONArray(hValue);

            //get the low array
            jPath = "/chart/result[0]/indicators/quote[0]/low";
            String lValue = jReader.getValue(jPath, jsonData);
            lowArray = new JSONArray(lValue);

            //get the volume array
            jPath = "/chart/result[0]/indicators/quote[0]/volume";
            String vValue = jReader.getValue(jPath, jsonData);
            volumeArray = new JSONArray(vValue);

        } catch (JSONException e) {
            System.out.println("something missing");
        }
            //global symbol elements
            //misc
            jPath = "/chart/result[0]/meta/symbol";
            value = jReader.getValue(jPath, jsonData);
            min_quote.setTicker(value);
            min_quote.setSource("Y");
            jPath = "/chart/result[0]/meta/exchangeTimezoneName";
            value = jReader.getValue(jPath, jsonData);
            min_quote.setTz(value);

            // System.out.println(min_quote.getTicker());
            // System.out.println(min_quote.getTz());


        //loop thru array
        for (int i=0; i<timeArray.length(); i++) {
            //System.out.println(i + " time: " + timeArray.getString(i) + " open: " + openArray.getString(i));

            //handle timestamp conversion
            if((!timeArray.isNull(i)) && (!b_dirty)) {
                day = ymutil.unix2dateUTC(Long.parseLong(timeArray.getString(i)));
                date = ymutil.unixtodate(Long.parseLong(timeArray.getString(i)));

                //System.out.println(i+ " " +day + " " + date);

                try {
                    //get quote elements
                    if (!openArray.isNull(i)) {
                        min_quote.setOpen(Double.parseDouble(openArray.getString(i)));
                    } else {
                        b_dirty = true;
                    }
                } catch (NullPointerException np) {
                    System.out.println("missing something in open");
                    b_dirty = true;
                }

                try {
                    if (!highArray.isNull(i)) {
                        min_quote.setHigh(Double.parseDouble(highArray.getString(i)));
                    } else {
                        b_dirty = true;
                    }
                } catch (NullPointerException np) {
                    System.out.println("missing something in high");
                    b_dirty = true;
                }

                try {
                    if (!lowArray.isNull(i)) {
                        min_quote.setLow(Double.parseDouble(lowArray.getString(i)));
                    } else {
                        b_dirty = true;
                    }
                } catch (NullPointerException np) {
                    System.out.println("missing something in low");
                    b_dirty = true;
                }

                try {
                    if (!closeArray.isNull(i)) {
                        min_quote.setClose(Double.parseDouble(closeArray.getString(i)));
                    } else {
                        b_dirty = true;
                    }
                } catch (NullPointerException np) {
                    System.out.println("missing something in close");
                    b_dirty = true;
                }

                try {
                    if (!volumeArray.isNull(i)) {
                        min_quote.setVolume(Long.parseLong(volumeArray.getString(i)));
                    } else {
                        b_dirty = true;
                    }

                } catch (NumberFormatException np) {
                    System.out.println("missing something in volume");
                    b_dirty = true;
                }

                min_quote.setDay(day);
                min_quote.setType("M");

                //build the _id for the data bar.
                String bar_id = min_quote.getTicker() + ":" + min_quote.getSource() + ":" + date;
                min_quote.setH_id(bar_id);

                //load mongo

                if(!b_dirty) {  //do not store dirty data
                    //if 1600 quote of the day append to prev bar
                    if (date.contains("1600")) {
                        min_quote.setH_id(prev_quote.getH_id()); //assign last bar id
                        min_quote.setDay(prev_quote.getDay());
                        ml.mongo_store_bar(min_quote, true); //append this data to prev bar
                    } else {
                        //Store every minute bar we build
                        ml.mongo_store_bar(min_quote, false); //just insert/replace the data
                    } //end if 1600
                } //end not dirty

                //make a copy of the prev index info
                prev_quote.setH_id(min_quote.getH_id());
                prev_quote.setDay(min_quote.getDay());

                min_quote.init();
                b_dirty = false;
            } //end if timestamp null

        } //end for timeArray


    } //end process y json
} // end class
